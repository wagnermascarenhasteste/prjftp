<html>
    <head>
        <title>  </title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <form >
            <table border="2" align="center">
                <tr>
                    <th>
                        Ingredientes
                    </th>
                    <th>
                        Peso B. (em Kg)
                    </th>
                    <th>
                        Vlr. Carboidrato
                    </th>
                    <th>
                        Vlr. Proteína
                    </th>
                    <th>
                        Vlr. Kcal
                    </th>
                </tr>
                <?php
                $handle = @fopen("leitura.txt", "r");
                if ($handle) {
                    while (($buffer = fgets($handle, 4096)) !== false) {
                        //echo $buffer."<br>";
                        $ingrediente = "";
                        $peso = "";
                        $carbo = "";
                        $proteina = "";
                        $kal = "";
                        list($ingrediente, $peso, $carbo, $proteina, $kal) = explode(";", $buffer);
                        ?>
                        <tr align="center">
                            <td>
                                <?php echo $ingrediente; ?>
                            </td>
                            <td>
                                <?php echo $peso; ?>
                            </td>
                            <td>
                                <?php echo $carbo; ?>
                            </td>
                            <td>
                                <?php echo $proteina; ?>
                            </td>
                            <td>
                                <?php echo $kal; ?>
                            </td>
                        </tr>

                        <?php
                    }
                    if (!feof($handle)) {
                        echo "Erro: falha inexperada de fgets()\n";
                    }

                    fclose($handle);
                }
                ?>
            </table>
        </form>

    </body>
</html>
